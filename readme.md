# Build patched Jboss EAP rpms

## About

The rpm changelog will have the description of the CVE applied.

The jboss patch system uses overlays, so the more patch you have, the more the rpm package size will grow.

The rpm version is incremented with each cumulative patch.

The release version is incremented with each standard patch.

Example :

```
* Mon Oct 27 2014 Version 4.2
- Patch BZ1114514.zip
- CVE-2014-3530

* Mon Oct 27 2014 Version 4.1
- Patch BZ1113339.zip
- CVE-2014-0075, CVE-2014-0096, CVE-2014-0099, CVE-2014-0119

* Mon Oct 27 2014 Version 4.0
- Patch jboss-eap-6.2.4-patch.zip
- Cumulative patch CP04 - CVE-2014-0109, CVE-2014-0110, CVE-2014-0034, CVE-2014-0035, CVE-2014-3481, CVE-2014-3481

* Mon Oct 27 2014 Version 3.0
- Patch jboss-eap-6.2.3-patch.zip
- Cumulative patch CP03 - CVE-2014-0059

* Mon Oct 27 2014 Version 2.0
- Patch jboss-eap-6.2.2.zip
- Cumulative patch CP02 - CVE-2013-4286, CVE-2014-0093

* Mon Oct 27 2014 Version 1.0
- Patch jboss-eap-6.2.1.zip
- Cumulative patch CP01 - CVE-2013-6440, CVE-2013-4517, CVE-2014-0018

* Mon Oct 27 2014 Version 0.0
- Base version

```

## Requirements

Build on el6.

You need to install the packages `p7zip` (from EPEL), `java-1.7.0-openjdk` and `rpm-build`

Better run as root, unless you set some extensive sudo rules.

## How to

Provide the zipped EAP src from  http://www.jboss.org/products/eap/download

In the src subfolder

Example :

```
.
`-- src
    |-- jboss-eap-6.2.0.zip
    `-- jboss-eap-6.3.0.zip

```

Provide the selected patchs from

https://access.redhat.com/jbossnetwork/restricted/listSoftware.html?downloadType=distributions&product=appplatform&productChanged=yes

In the src/patchs-<jbver> subfolder.

Example :

```
.
`-- src
    |-- patchs-6.2.0
    |   |-- BZ1113339.zip
    |   |-- BZ1114514.zip
    |   |-- jboss-eap-6.2.1.zip
    |   |-- jboss-eap-6.2.2.zip
    |   |-- jboss-eap-6.2.3-patch.zip
    |   |-- jboss-eap-6.2.4-patch.zip
    |   `-- jboss-ews-2.0.1-CVE-2014-patch-bundle-4-el6-x86_64.zip
    `-- patchs-6.3.0
        `-- jboss-eap-6.3.1-patch.zip
```

Set a patch list file patchs-<jbver>.lst

One patch per line.

The patchs must be in the apply order, from older to newer.

A line is  : zip file name; patch description.

A cumulative patch MUST have the word "cumulative" in the description.

Otherwise you will scramble the rpm versions.

Example : `patchs-6.2.0.lst`

```
jboss-eap-6.2.1.zip; Cumulative patch CP01 - CVE-2013-6440, CVE-2013-4517, CVE-2014-0018
jboss-eap-6.2.2.zip; Cumulative patch CP02 - CVE-2013-4286, CVE-2014-0093
jboss-eap-6.2.3-patch.zip; Cumulative patch CP03 - CVE-2014-0059
jboss-eap-6.2.4-patch.zip; Cumulative patch CP04 - CVE-2014-0109, CVE-2014-0110, CVE-2014-0034, CVE-2014-0035, CVE-2014-3481, CVE-2014-3481
BZ1113339.zip; CVE-2014-0075, CVE-2014-0096, CVE-2014-0099, CVE-2014-0119
BZ1114514.zip; CVE-2014-3530

```

Then just edit `VER` in the build script, and launch.

## Example of run


```
Unzip src
Set the spec file
Build the rpm
Result :
/home/lofic/rpmjbbuild/RPMS/noarch/jboss-eap-6.2.0-0-0.el6.noarch.rpm
/home/lofic/rpmjbbuild/SRPMS/jboss-eap-6.2.0-0-0.el6.src.rpm
Install the rpm
Préparation...              ########################################### [100%]
   1:jboss-eap-6.2.0        ########################################### [100%]
Start Jboss
usermod : aucun changement
Wait 5 sec for restart
nohup: redirige stderr sur stdout
Test that the CLI is up
Not yet. Wait 5 sec more
Test that the CLI is up
Apply patch : jboss-eap-6.2.1.zip
Desc : Cumulative patch CP01 - CVE-2013-6440, CVE-2013-4517, CVE-2014-0018
{
    "outcome" : "success",
    "response-headers" : {
        "operation-requires-restart" : true,
        "process-state" : "restart-required"
    }
}
Restart Jboss
{"outcome" => "success"}
Wait 5 sec for restart
Test that the CLI is up
Test the Jboss version
JBoss AS product: EAP 6.2.1.GA
Apply patch : jboss-eap-6.2.2.zip
Desc : Cumulative patch CP02 - CVE-2013-4286, CVE-2014-0093
{
    "outcome" : "success",
    "response-headers" : {
        "operation-requires-restart" : true,
        "process-state" : "restart-required"
    }
}
Restart Jboss
{"outcome" => "success"}
Wait 5 sec for restart
Test that the CLI is up
Test the Jboss version
JBoss AS product: EAP 6.2.2.GA
Apply patch : jboss-eap-6.2.3-patch.zip
Desc : Cumulative patch CP03 - CVE-2014-0059
{
    "outcome" : "success",
    "response-headers" : {
        "operation-requires-restart" : true,
        "process-state" : "restart-required"
    }
}
Restart Jboss
{"outcome" => "success"}
Wait 5 sec for restart
Test that the CLI is up
Test the Jboss version
JBoss AS product: EAP 6.2.3.GA
Apply patch : jboss-eap-6.2.4-patch.zip
Desc : Cumulative patch CP04 - CVE-2014-0109, CVE-2014-0110, CVE-2014-0034, CVE-2014-0035, CVE-2014-3481, CVE-2014-3481
{
    "outcome" : "success",
    "response-headers" : {
        "operation-requires-restart" : true,
        "process-state" : "restart-required"
    }
}
Restart Jboss
{"outcome" => "success"}
Wait 5 sec for restart
Test that the CLI is up
Test the Jboss version
JBoss AS product: EAP 6.2.4.GA
Apply patch : BZ1113339.zip
Desc : CVE-2014-0075, CVE-2014-0096, CVE-2014-0099, CVE-2014-0119
{
    "outcome" : "success",
    "response-headers" : {
        "operation-requires-restart" : true,
        "process-state" : "restart-required"
    }
}
Restart Jboss
{"outcome" => "success"}
Wait 5 sec for restart
Test that the CLI is up
Test the Jboss version
JBoss AS product: EAP 6.2.4.GA
Apply patch : BZ1114514.zip
Desc : CVE-2014-3530
{
    "outcome" : "success",
    "response-headers" : {
        "operation-requires-restart" : true,
        "process-state" : "restart-required"
    }
}
Restart Jboss
{"outcome" => "success"}
Wait 5 sec for restart
Test that the CLI is up
Test the Jboss version
JBoss AS product: EAP 6.2.4.GA
Stop Jboss
{"outcome" => "success"}
Zip the patched version
Uninstall the rpm
Build the rpm
Result :
/home/lofic/rpmjbbuild/RPMS/noarch/jboss-eap-6.2.0-4-2.el6.noarch.rpm
/home/lofic/rpmjbbuild/SRPMS/jboss-eap-6.2.0-4-2.el6.src.rpm

```

